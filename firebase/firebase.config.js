import { initializeApp } from "firebase/app";
import { getFirestore, doc, updateDoc } from 'firebase/firestore';
import {getStorage} from 'firebase/storage';


const firebaseConfig = {
  apiKey: "AIzaSyDy9SoIVdX-1fZSGbdko_rOLvXbVwGHm0U",
  authDomain: "server-binar-chapter-10.firebaseapp.com",
  projectId: "server-binar-chapter-10",
  storageBucket: "server-binar-chapter-10.appspot.com",
  messagingSenderId: "210538881061",
  appId: "1:210538881061:web:7c4d6a9c6c57197f399fcc"

};


const app = initializeApp(firebaseConfig);
const storageFire = getStorage(app);
const db = getFirestore(app);
export {app, storageFire, db}

export async function setGameWin(userId, data ) {
  const washingtonRef = doc(db, "player-game", userId);
  await updateDoc(washingtonRef, {
    score: data
  });
}

export async function setGamePlay(userId, data ) {
  const washingtonRef = doc(db, "player-game", userId);
  await updateDoc(washingtonRef, {
    play: data
  });
}