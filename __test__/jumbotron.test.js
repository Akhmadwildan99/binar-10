/**
 * @jest-environment jsdom
 */
import React from 'react';
import {render} from '@testing-library/react';
import Jumbotron from '../components/jumbotron/index';

it("jumbotron shoul have Play Traditional Game text", () => {
    const {queryAllByText} = render(<Jumbotron />);
    
    expect(queryAllByText("Play Traditional Game")).toBeTruthy();
    expect(queryAllByText("Experience new traditional game")).toBeTruthy();
});



