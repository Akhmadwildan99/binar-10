/**
 * @jest-environment jsdom
 */

 import React from 'react';
 import {render} from '@testing-library/react';
 import Carousel from '../components/carousel/index';

 it('should have img element', () => {
     const {queryByText} = render(<Carousel />);

     expect(queryByText('Whats so special?')).toBeTruthy();
     expect(queryByText('The games')).toBeTruthy();
 })