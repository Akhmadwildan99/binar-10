/**
 * @jest-environment jsdom
 */

import React from 'react';
import {screen, render} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Bodyregister from '../components/bodyRegister/index'

test('types into the input', () => {
    render(<Bodyregister />);

    const inputEmail =  screen.getByPlaceholderText('Email');
    userEvent.type(inputEmail, 'agus@gmail.com')
    expect(inputEmail.value).toBe('agus@gmail.com');

    const inputPassword =  screen.getByPlaceholderText('Password');
    userEvent.type(inputPassword, 'agus')
    expect(inputPassword.value).toBe('agus');
})