import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { useRouter } from 'next/router';
import ListGame from '../components/GameList';

const GameList = props => {
  const router = useRouter();

  useEffect(() => {
    if (props.isLogin === false) {
      router.push('/login');
    }
  });

  return <>{props.isLogin && <ListGame />};</>;
};

const mapStateToProps = state => {
  return {
    isLogin: state.reducUser.isLogin,
    user: state.reducUser.user,
  };
};

export default connect(mapStateToProps, null)(GameList);
