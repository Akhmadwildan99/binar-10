import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { useRouter } from 'next/router';
import Profile from '../components/Profile';

const ProfilePage = props => {
  const router = useRouter();

  useEffect(() => {
    console.log(props.playerData);
    if (!props.playerData.name) {
      router.push('/playerGame');
    }
  }, []);

  return <>{props.playerData && <Profile />};</>;
};

const mapStateToProps = state => {
  return {
    isLogin: state.reducUser.isLogin,
    user: state.reducUser.user,
    setProfile: state.reducUser.setProfile,
    playerData: state.reducUser.playerData,
  };
};

export default connect(mapStateToProps, null)(ProfilePage);
