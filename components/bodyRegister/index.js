import {useState, useEffect} from 'react'
import Button from '../Button/button';
import {useSelector, useDispatch} from 'react-redux';
import {registerAPI} from '../../redux/action';


function Bodyregister() {
    const dispatch = useDispatch();
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [register, setRegister] = useState(false);

    const {isLoading} = useSelector(state => state.reducUser)
    
    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            if(email !== "" && password !== "") {
                const data ={email, password};
                await dispatch(registerAPI(data));
                setTimeout(() => {
                    setEmail("");
                    setPassword("");
                    setRegister(true);
                }, 2000)
                // router.push('/login');
            } 
        } catch (err) {
            setRegister(false);
            console.log(err);
        }
       
    }
    useEffect(() => {
        if(register === true) {
            setEmail("");
            setPassword("");
        }
    }, [register]);

    return (
        <div className="body-bg">
            <div className="form-container">
                <div className="form">
                    <h1>Register</h1>
                    <form >
                        <input 
                        type="text" 
                        placeholder="Email" 
                        name="email"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)} 
                        required />
                        <input 
                        type="password" 
                        placeholder="Password" 
                        name="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)} 
                        required />
                        <Button onClick={handleSubmit} title={"Register"} loading={isLoading} />
                    </form>
                </div>
                <div className="img-side"></div>
            </div>
        </div>
    )
}

export default Bodyregister;

