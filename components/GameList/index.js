import React from "react";
import Navbar from  "../navbar";
import List from "./list";

const GameList = () => {
    return(
        <React.Fragment> 
            <Navbar/>
            <List/>
        </React.Fragment> 
    );
};


export default GameList;