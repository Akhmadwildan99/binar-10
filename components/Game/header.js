import Link from 'next/link'

export default function Header({ronde, play, gameQuitSubmit, currentScore}) {
    return (
        <header className="header">
          <ul>
              <Link href="/" ><a onClick={gameQuitSubmit}><li className="navigasi"> &laquo; </li></a></Link>
              <a><li className="logo"><img src="/images/logo 1.jpg" alt=""/></li></a>
              <a><li className="title">batu kertas gunting</li></a>
              <a><li className="title-main">Ronde: {ronde} </li></a>
              {play ? 
              <a><li className="title-main">Main: {play} </li></a> :
              <a><li className="title-main">Belum Pernah main</li></a>}
              {currentScore && <a><li className="title-main">Score: {currentScore} </li></a> }
              
          </ul>
        </header>
    )
}
