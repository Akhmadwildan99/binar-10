import React from "react";
import Header from "./Header";
import ProfileSummary from "./ProfileSummary";
import ProfileContent from "./ProfileContent";

const Profile = () => {
  return (
    <React.Fragment>
      <Header />
      <ProfileSummary />
      <ProfileContent />
    </React.Fragment>
  );
};

export default Profile;
