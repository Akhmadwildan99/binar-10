import React from "react";
import Navbar from "../../navbar";

import Image from "next/image";
import imgHeader from "./header.png";
import classes from "./Header.module.css";

const Header = () => {
  return (
    <React.Fragment>
      <Navbar />
      <div className={classes["main-image"]}>
        <Image src={imgHeader} alt="header-background" />
      </div>
    </React.Fragment>
  );
};

export default Header;
