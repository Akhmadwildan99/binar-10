import React from "react";

import classes from "./ProfileContent.module.css";
import ProfileDetail from "./ProfileDetail";
import ProfileScore from "./ProfileSocre";

const ProfileContent = () => {
  return (
    <section className={`${classes["box-container"]} row`}>
      <ProfileDetail />
      <ProfileScore />
    </section>
  );
};

export default ProfileContent;
