import React from 'react';

import classes from './ProfileScore.module.css';
import ScoreList from './ScoreList';

const ProfileScore = () => {
  const renderScoreList = () => {
    return <ScoreList />;
  };

  return (
    <div className="col-5">
      <div className={classes.score}>
        <h4>Total Score</h4>
        <hr />
        <ul>{renderScoreList()}</ul>
        <hr />
        <div>
          <h5>Total Points:</h5>
        </div>
      </div>
    </div>
  );
};

export default ProfileScore;
