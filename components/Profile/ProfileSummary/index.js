import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import { fetchPlayer } from '../../../redux/action';
import classes from './ProfileSummary.module.css';

const ProfileSummary = props => {
  useEffect(() => {
    props.fetchPlayer(props.user.uid);
  }, []);

  return (
    <section className={classes.summary}>
      <img
        className="img-thumbnail"
        src={
          props.playerData.imageUrl
            ? props.playerData.imageUrl
            : '/public/images/avatar.png'
        }
        alt="avatar"
      />
      <h2>{props.playerData.name}</h2>
      <p>{props.playerData.bio}</p>
    </section>
  );
};

const mapStateToProps = state => {
  return {
    isLogin: state.reducUser.isLogin,
    user: state.reducUser.user,
    playerData: state.reducUser.playerData,
  };
};

export default connect(mapStateToProps, { fetchPlayer })(ProfileSummary);
