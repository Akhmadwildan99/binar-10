import {
  PRO_LOADING,
  PRO_LOGIN,
  CHANGE_USER,
  FETCH_PLAYER,
  UPDATE_PLAYER,
  SET_PROFILE,
} from './typeAction';

const userInfo = {
  isLoading: false,
  isLogin: false,
  setProfile: false,
  user: {},
  playerData: {},
  score: 0,
};

const reducUser = (state = userInfo, action) => {
  if (action.type === PRO_LOADING) {
    return {
      ...state,
      isLoading: action.value,
    };
  }

  if (action.type === PRO_LOGIN) {
    return {
      ...state,
      isLogin: action.value,
    };
  }

  if (action.type === CHANGE_USER) {
    return {
      ...state,
      user: action.value,
    };
  }
  if (action.type === FETCH_PLAYER) {
    return {
      ...state,
      playerData: action.payload,
    };
  }
  if (action.type === UPDATE_PLAYER) {
    return {
      ...state,
      res: action.payload,
    };
  }
  if (action.type === SET_PROFILE) {
    return {
      ...state,
      setProfile: action.payload,
    };
  }
  return state;
};

export default reducUser;
