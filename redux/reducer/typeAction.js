export const PRO_LOADING = 'PRO_LOADING';

export const PRO_LOGIN = 'PRO_LOGIN';

export const SET_PROFILE = 'SET_PROFILE';

export const CHANGE_USER = 'CHANGE_USER';

export const FETCH_PLAYER = 'FETCH_PLAYER';

export const UPDATE_PLAYER = 'UPDATE_PLAYER';
