import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
} from 'firebase/auth';
import { setDoc, getDoc, updateDoc, doc } from 'firebase/firestore';
import { app, db } from '../../firebase/firebase.config';
import {
  PRO_LOADING,
  PRO_LOGIN,
  CHANGE_USER,
  FETCH_PLAYER,
  UPDATE_PLAYER,
  SET_PROFILE,
} from '../reducer/typeAction.js';

const registerAPI = data => dispatch => {
  return new Promise((resolve, reject) => {
    dispatch({ type: PRO_LOADING, value: true });
    const auth = getAuth(app);
    createUserWithEmailAndPassword(auth, data.email, data.password)
      .then(userCredential => {
        const user = userCredential.user;
        console.log('user', user);
        dispatch({ type: PRO_LOADING, value: false });
        resolve();
      })
      .catch(error => {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log('error', errorCode, errorMessage);
        dispatch({ type: PRO_LOADING, value: false });
        reject();
      });
  });
};

const loginAPI = data => dispatch => {
  dispatch({ type: PRO_LOADING, value: true });
  new Promise((resolve, reject) => {
    const auth = getAuth(app);
    signInWithEmailAndPassword(auth, data.email, data.password)
      .then(res => {
        const dataUser = {
          user: res.user.email,
          uid: res.user.uid,
        };
        dispatch({ type: PRO_LOADING, value: false });
        dispatch({ type: PRO_LOGIN, value: true });
        dispatch({ type: CHANGE_USER, value: dataUser });
        dispatch(fetchPlayer(dataUser.uid));
        resolve();
      })
      .catch(error => {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log('error', errorCode, errorMessage);
        dispatch({ type: PRO_LOADING, value: false });
        dispatch({ type: PRO_LOGIN, value: false });
        reject();
      });
  });
};

const submitPlayer = (userId, data) => dispatch => {
  dispatch({ type: PRO_LOADING, value: true });
  new Promise((resolve, reject) => {
    setDoc(doc(db, 'player-game', userId), data)
      .then(res => {
        console.log(res);
        dispatch({ type: PRO_LOADING, value: false });
        dispatch({ type: SET_PROFILE, payload: true });
        resolve();
      })
      .catch(err => {
        console.log('error', err);
        dispatch({ type: PRO_LOADING, value: false });
        dispatch({ type: SET_PROFILE, payload: false });
        reject();
      });
  });
};

const fetchPlayer = userId => async dispatch => {
  const res = await getDoc(doc(db, 'player-game', userId));

  dispatch({ type: FETCH_PLAYER, payload: res.data() });
};

const updateplayer = (userId, updatedData) => async dispatch => {
  const res = await updateDoc(doc(db, 'player-game', userId), updatedData);

  dispatch({ type: UPDATE_PLAYER, payload: res });
};

export { registerAPI, loginAPI, submitPlayer, fetchPlayer, updateplayer };
